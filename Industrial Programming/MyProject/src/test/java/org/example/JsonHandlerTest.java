package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonHandlerTest {

    @Test
    public void testConvertJsonToList() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray(Arrays.asList("2+3", "4*5", "6/2", "7-1", "8%3"));
        jsonObject.put("expressions", jsonArray);

        List<String> expected = Arrays.asList("2+3", "4*5", "6/2", "7-1", "8%3");
        List<String> result = JsonHandler.convertJsonToList(jsonObject);

        assertEquals(expected, result, "The list converted from JSON does not match the expected results");
    }

    @Test
    public void testConvertListToJson() {
        List<String> data = Arrays.asList("5.0", "20.0", "3.0", "6.0", "2.0");
        JSONObject expected = new JSONObject();
        JSONArray jsonArray = new JSONArray(data);
        expected.put("result", jsonArray);

        JSONObject result = JsonHandler.convertListToJson(data);

        assertEquals(expected.toString(), result.toString(), "The JSON converted from list does not match the expected results");
    }
}
