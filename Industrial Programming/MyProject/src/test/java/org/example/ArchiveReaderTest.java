package org.example;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArchiveReaderTest {

    @TempDir
    Path tempDir;

    @Test
    public void testReadZipFile() throws Exception {
        File tempFile = tempDir.resolve("tempFile.zip").toFile();

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(tempFile))) {
            ZipEntry zipEntry = new ZipEntry("test.json");
            zos.putNextEntry(zipEntry);

            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray(Arrays.asList("2+3", "4*5", "6/2", "7-1", "8%3"));
            jsonObject.put("expressions", jsonArray);

            zos.write(jsonObject.toString().getBytes());
            zos.closeEntry();
        }

        List<String> expected = Arrays.asList("2+3", "4*5", "6/2", "7-1", "8%3");
        List<String> result = ArchiveReader.readZipFile(tempFile.getPath());

        assertEquals(expected, result, "The list read from zip file does not match the expected results");
    }
}
