package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CryptoHandlerTest {

    @TempDir
    Path tempDir;

    @Test
    public void testEncryptDecrypt() throws Exception {
        File tempFile = tempDir.resolve("tempFile.txt").toFile();
        File encryptedFile = tempDir.resolve("encryptedFile.txt").toFile();
        File decryptedFile = tempDir.resolve("decryptedFile.txt").toFile();

        String expected = "Hello, World!";
        Files.write(tempFile.toPath(), expected.getBytes());

        String key = CryptoHandler.generateKey();

        CryptoHandler.encrypt(key, tempFile, encryptedFile);

        CryptoHandler.decrypt(key, encryptedFile, decryptedFile);

        String result = new String(Files.readAllBytes(decryptedFile.toPath()));
        assertEquals(expected, result, "The decrypted string does not match the original string");
    }

    @Test
    public void testZipUnzip() throws Exception {
        File tempFile = tempDir.resolve("tempFile.txt").toFile();
        File zippedFile = tempDir.resolve("zippedFile.zip").toFile();
        File unzippedFile = tempDir.resolve("unzippedFile.txt").toFile();

        String expected = "Hello, World!";
        Files.write(tempFile.toPath(), expected.getBytes());

        CryptoHandler.zip(tempFile, zippedFile);

        CryptoHandler.unzip(zippedFile, unzippedFile);

        String result = new String(Files.readAllBytes(unzippedFile.toPath()));
        assertEquals(expected, result, "The unzipped string does not match the original string");
    }
}
