package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipFile;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ArchiveHandlerTest {

    @TempDir
    Path tempDir;

    @Test
    public void testArchiveFile() throws Exception {
        File tempFile = tempDir.resolve("tempFile.txt").toFile();
        File archiveFile = new File(tempFile.getPath() + ".zip");

        String expected = "Hello, World!";
        Files.write(tempFile.toPath(), expected.getBytes());

        ArchiveHandler.archiveFile(tempFile.getPath());

        assertTrue(archiveFile.exists(), "The archive file does not exist");

        try (ZipFile zipFile = new ZipFile(archiveFile)) {
            assertEquals(1, zipFile.size(), "The archive file does not contain the expected number of entries");
        }
    }
}
