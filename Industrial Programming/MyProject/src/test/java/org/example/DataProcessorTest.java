package org.example;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DataProcessorTest {
    @Test
    public void testProcessData() {
        List<String> data = Arrays.asList("2+3", "4*5", "6/2", "7-1", "8%3");
        List<String> expected = Arrays.asList("5.0", "20.0", "3.0", "6.0", "2.0");

        List<String> result = DataProcessor.processData(data);

        assertEquals(expected, result, "The processed data does not match the expected results");
    }

    @Test
    public void testProcessDataWithNegativeNumbers() {
        List<String> data = Arrays.asList("-2+3", "4*-5", "-6/2", "7--1", "-8%3");
        List<String> expected = Arrays.asList("1.0", "-20.0", "-3.0", "8.0", "-2.0");

        List<String> result = DataProcessor.processData(data);

        assertEquals(expected, result, "The processed data does not match the expected results");
    }

    @Test
    public void testProcessDataWithDecimalNumbers() {
        List<String> data = Arrays.asList("2.5+3.5", "4.2*5.0", "6.6/2.0", "7.7-1.1", "8%3");
        List<String> expected = Arrays.asList("6.0", "21.0", "3.3", "6.6", "2.0");

        List<String> result = DataProcessor.processData(data);

        assertEquals(expected, result, "The processed data does not match the expected results");
    }

}