package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class XmlHandlerTest {

    @Test
    public void testConvertXmlToList() {
        Document doc = new Document();
        Element rootElement = new Element("expressions");
        doc.setRootElement(rootElement);

        List<String> expected = Arrays.asList("2+3", "4*5", "6/2", "7-1", "8%3");
        for (String line : expected) {
            Element expression = new Element("expression");
            expression.setText(line);
            rootElement.addContent(expression);
        }

        List<String> result = XmlHandler.convertXmlToList(doc);

        assertEquals(expected, result, "The list converted from XML does not match the expected results");
    }

    @Test
    public void testConvertListToXmlWithEmptyString() {
        List<String> data = Arrays.asList("");
        Document expected = new Document();
        Element rootElement = new Element("result");
        expected.setRootElement(rootElement);

        Element expression = new Element("result");
        expression.setText("");
        rootElement.addContent(expression);

        Document result = XmlHandler.convertListToXml(data);

        XMLOutputter xmlOutputter = new XMLOutputter(Format.getCompactFormat());
        String expectedXml = xmlOutputter.outputString(expected);
        String resultXml = xmlOutputter.outputString(result);

        Assertions.assertEquals(expectedXml, resultXml, "The XML converted from list does not match the expected results");
    }
}
