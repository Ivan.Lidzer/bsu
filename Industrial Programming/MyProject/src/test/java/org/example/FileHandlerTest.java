package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.jdom2.Document;
import org.jdom2.Element;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileHandlerTest {

    @TempDir
    Path tempDir;

    @Test
    public void testReadWriteFile() throws Exception {
        File tempFile = tempDir.resolve("tempFile.txt").toFile();
        List<String> expected = Arrays.asList("Hello", "World");
        FileHandler.writeFile(tempFile.getPath(), expected);
        List<String> result = FileHandler.readFile(tempFile.getPath());
        assertEquals(expected, result, "The list read from file does not match the expected results");
    }

    @Test
    public void testReadWriteJsonFile() throws Exception {
        File tempFile = tempDir.resolve("tempFile.json").toFile();

        JSONObject innerObject = new JSONObject()
                .put("innerKey1", "innerValue1")
                .put("innerKey2", "innerValue2");

        JSONArray jsonArray = new JSONArray(Arrays.asList("item1", "item2", "item3"));

        JSONObject expected = new JSONObject()
                .put("key", "value")
                .put("object", innerObject)
                .put("array", jsonArray);

        FileHandler.writeJsonFile(tempFile.getPath(), expected);
        JSONObject result = FileHandler.readJsonFile(tempFile.getPath());

        assertEquals(expected.toString(), result.toString(), "The JSON read from file does not match the expected results");
    }

    @Test
    public void testReadWriteXmlFile() throws Exception {
        File tempFile = tempDir.resolve("tempFile.xml").toFile();
        Document expected = new Document(new Element("root"));
        FileHandler.writeXmlFile(tempFile.getPath(), expected);
        Document result = FileHandler.readXmlFile(tempFile.getPath());
        assertEquals(expected.getRootElement().getName(), result.getRootElement().getName(), "The XML read from file does not match the expected results");
    }
}

