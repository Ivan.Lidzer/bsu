package org.example;

import org.jdom2.Document;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import static org.example.FileHandler.readXmlFile;
import static org.example.FileHandler.writeXmlFile;

public class MainGUI extends JFrame {
    private JComboBox<String> inputFormatField;
    private JTextField inputFileField;
    private JCheckBox encryptOutputField;
    private JCheckBox archiveOutputField;
    private JComboBox<String> outputFormatField;
    private JTextField outputFileField;
    private JTextField decryptionKeyField;
    private JCheckBox decryptOutputField;
    private String encryptionKey;

    public MainGUI() {
        createGUI();
    }

    private void createGUI() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(new JLabel("Выберите формат входного файла:"));
        String[] inputFormats = {"txt", "json", "xml", "zip"};
        inputFormatField = new JComboBox<>(inputFormats);
        panel.add(inputFormatField);

        panel.add(new JLabel("Выберите входной файл:"));
        inputFileField = new JTextField();
        JButton inputFileButton = new JButton("Выбрать файл");
        inputFileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    inputFileField.setText(selectedFile.getAbsolutePath());
                }
            }
        });
        panel.add(inputFileField);
        panel.add(inputFileButton);

        panel.add(new JLabel("Шифровать выходные данные?"));
        encryptOutputField = new JCheckBox();
        panel.add(encryptOutputField);

        panel.add(new JLabel("Архивировать выходные данные?"));
        archiveOutputField = new JCheckBox();
        panel.add(archiveOutputField);


        panel.add(new JLabel("Выберите формат выходного файла:"));
        String[] outputFormats = {"txt", "json", "xml"};
        outputFormatField = new JComboBox<>(outputFormats);
        panel.add(outputFormatField);

        panel.add(new JLabel("Введите имя выходного файла:"));
        outputFileField = new JTextField();
        panel.add(outputFileField);

        JButton button = new JButton("Запустить");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    runMain();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        panel.add(button);

        decryptOutputField = new JCheckBox("Расшифровать выходные данные?");
        panel.add(decryptOutputField);

        decryptionKeyField = new JTextField();
        decryptionKeyField.setEnabled(false);
        panel.add(decryptionKeyField);

        decryptOutputField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decryptionKeyField.setEnabled(decryptOutputField.isSelected());
            }
        });

        getContentPane().add(panel);
        pack();
        setVisible(true);
    }

    private void runMain() throws Exception {
        String inputFormat = (String) inputFormatField.getSelectedItem();
        String inputFile = inputFileField.getText();
        boolean encryptOutput = encryptOutputField.isSelected();
        boolean archiveOutput = archiveOutputField.isSelected();
        String outputFormat = (String) outputFormatField.getSelectedItem();
        String outputFile = outputFileField.getText();

        if ("json".equals(outputFormat)) {
            outputFile += ".json";
        } else if ("xml".equals(outputFormat)) {
            outputFile += ".xml";
        } else {
            outputFile += ".txt";
        }

        List<String> data;

        if ("json".equals(inputFormat)) {
            JSONObject jsonObject = FileHandler.readJsonFile(inputFile);
            data = JsonHandler.convertJsonToList(jsonObject);
        } else if ("xml".equals(inputFormat)) {
            Document doc = readXmlFile(inputFile);
            data = XmlHandler.convertXmlToList(doc);
        } else if ("zip".equals(inputFormat)) {
            data=ArchiveReader.readZipFile(inputFile);
        }
        else {
            data = FileHandler.readFile(inputFile);
        }

        List<String> processedData = DataProcessor.processData(data);

        if ("json".equals(outputFormat)) {
            JSONObject jsonObject = JsonHandler.convertListToJson(processedData);
            FileHandler.writeJsonFile(outputFile, jsonObject);
        } else if ("xml".equals(outputFormat)) {
            Document doc = XmlHandler.convertListToXml(processedData);
            writeXmlFile(outputFile, doc);
        } else {
            FileHandler.writeFile(outputFile, processedData);
        }

        if (encryptOutput) {
            encryptionKey = CryptoHandler.generateKey();
            File encryptedFile = new File("encrypted_" + outputFile);
            CryptoHandler.encrypt(encryptionKey, new File(outputFile), encryptedFile);
            outputFile = encryptedFile.getName();
            decryptionKeyField.setText(encryptionKey);
        }

        if (decryptOutputField.isSelected()) {
            String decryptionKey = decryptionKeyField.getText();
            File decryptedFile = new File("decrypted_" + outputFile);
            CryptoHandler.decrypt(decryptionKey, new File(outputFile), decryptedFile);
            System.out.println("Файл успешно расшифрован!");
        }

        if (archiveOutput) {
            ArchiveHandler.archiveFile(outputFile);
            new File(outputFile).delete();
        }

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new MainGUI();
            }
        });
    }
}
