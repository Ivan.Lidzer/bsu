package org.example;

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.List;

public class XmlHandler {
    public static List<String> convertXmlToList(Document doc) {
        List<String> list = new ArrayList<>();
        Element rootElement = doc.getRootElement();
        List<Element> elements = rootElement.getChildren("expression");

        for (Element element : elements) {
            list.add(element.getText());
        }

        return list;
    }

    public static Document convertListToXml(List<String> data) {
        Document doc = new Document();
        Element rootElement = new Element("result");
        doc.setRootElement(rootElement);

        for (String line : data) {
            Element expression = new Element("result");
            expression.setText(line);
            rootElement.addContent(expression);
        }

        return doc;
    }
}
