package org.example;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class Home {

    @GetMapping("/home")
    public String process(@RequestParam("homePar") String homePar) {
        // Вызовите ваши функции здесь
        if (Objects.equals(homePar, null)) {
            return "Home Page";

        }
        else {
            return "Home Page " + homePar;
        }
    }
}
