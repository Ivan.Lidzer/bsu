package org.example;

import org.jdom2.Document;
import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Scanner;

import static org.example.FileHandler.readXmlFile;
import static org.example.FileHandler.writeXmlFile;


public class Main {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        String inputFormat;
        String inputFile;

        do {
            System.out.println("Введите формат входного файла (txt, json, xml , zip):");
            inputFormat = scanner.nextLine();

            System.out.println("Введите имя входного файла:");
            inputFile = scanner.nextLine();

            if (!inputFile.endsWith("." + inputFormat)) {
                System.out.println("Ошибка: формат файла не соответствует его расширению! Попробуйте еще раз.");
            }
        } while (!inputFile.endsWith("." + inputFormat));


        System.out.println("Хотите ли вы шифровать выходные данные? (yes/no):");
        String encryptOutput = scanner.nextLine();

        System.out.println("Хотите ли вы архивировать выходные данные? (yes/no):");
        String archiveOutput = scanner.nextLine();

        System.out.println("Введите формат выходного файла (txt, json, xml):");
        String outputFormat = scanner.nextLine();

        System.out.println("Введите имя выходного файла:");
        String outputFile = scanner.nextLine();

        if ("json".equals(outputFormat)) {
            outputFile += ".json";
        } else if ("xml".equals(outputFormat)) {
            outputFile += ".xml";
        } else {
            outputFile += ".txt";
        }

        List<String> data;

        if ("json".equals(inputFormat)) {
            JSONObject jsonObject = FileHandler.readJsonFile(inputFile);
            data = JsonHandler.convertJsonToList(jsonObject);
        } else if ("xml".equals(inputFormat)) {
            Document doc = readXmlFile(inputFile);
            data = XmlHandler.convertXmlToList(doc);
        } else if ("zip".equals(inputFormat)) {
            data=ArchiveReader.readZipFile(inputFile);
        }
        else {
            data = FileHandler.readFile(inputFile);
        }

        List<String> processedData = DataProcessor.processData(data);

        if ("json".equals(outputFormat)) {
            JSONObject jsonObject = JsonHandler.convertListToJson(processedData);
            FileHandler.writeJsonFile(outputFile, jsonObject);
        } else if ("xml".equals(outputFormat)) {
            Document doc = XmlHandler.convertListToXml(processedData);
            writeXmlFile(outputFile, doc);
        } else {
            FileHandler.writeFile(outputFile, processedData);
        }


        if ("yes".equals(encryptOutput)) {
            String key = CryptoHandler.generateKey();
            System.out.println("Ключ шифрования: " + key);
            File encryptedFile = new File("encrypted_" + outputFile);
            CryptoHandler.encrypt(key, new File(outputFile), encryptedFile);
            outputFile = encryptedFile.getName();
        }

        System.out.println("Хотите ли вы расшифровать файл? (yes/no):");
        String decryptFile = scanner.nextLine();

        if ("yes".equals(decryptFile)) {
            System.out.println("Введите ключ шифрования:");
            String decryptionKey = scanner.nextLine();
            File decryptedFile = new File("decrypted_" + outputFile);
            CryptoHandler.decrypt(decryptionKey, new File(outputFile), decryptedFile);
            System.out.println("Файл был расшифрован.");
        }

        if ("yes".equals(archiveOutput)) {
            ArchiveHandler.archiveFile(outputFile);
            new File(outputFile).delete();
        }
    }
}
