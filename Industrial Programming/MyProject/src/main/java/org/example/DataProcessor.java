package org.example;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.ArrayList;
import java.util.List;

    class DataProcessor {
        public static List<String> processData(List<String> data) {
            List<String> processedData = new ArrayList<>();
            for (String line : data) {
                Expression expression = new ExpressionBuilder(line).build();
                double result = expression.evaluate();
                processedData.add(String.valueOf(result));
            }
            return processedData;
        }
    }
