package org.example;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileProcessorService {

    @GetMapping("/processFiles")
    public String processFiles(@RequestParam("inputFile") String inputFile,
                               @RequestParam("outputFile") String outputFile) {
        // Вызовите ваши функции здесь
        String result;
        try {
            result = YourClass.yourFunction(inputFile, outputFile);
        }
        catch(Exception error) {
            System.out.println(error.getMessage());
            return error.getMessage();
        }
        return result;

    }
}
