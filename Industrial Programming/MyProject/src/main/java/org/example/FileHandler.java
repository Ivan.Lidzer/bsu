package org.example;

import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
class FileHandler {
    public static List<String> readFile(String inputFile) {
        List<String> data = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                data.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static void writeFile(String outputFile, List<String> data) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
            for (String line : data) {
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject readJsonFile(String inputFile) {
        JSONObject jsonObject = null;
        try (FileReader reader = new FileReader(inputFile)) {
            JSONTokener tokener = new JSONTokener(reader);
            jsonObject = new JSONObject(tokener);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static void writeJsonFile(String outputFile, JSONObject jsonObject) {
        try (FileWriter file = new FileWriter(outputFile)) {
            file.write(jsonObject.toString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static org.jdom2.Document readXmlFile(String inputFile) {
        org.jdom2.Document doc = null;
        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            doc = saxBuilder.build(inputFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doc;
    }

    public static void writeXmlFile(String outputFile, org.jdom2.Document doc) {
        try {
            XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
            StringWriter writer = new StringWriter();
            xmlOutputter.output(doc, writer);
            String xmlString = writer.toString();
            FileHandler.writeFile(outputFile, Collections.singletonList(xmlString));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
