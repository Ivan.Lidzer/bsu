package org.example;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Scanner;

public class ArchiveReader {
    public static List<String> readZipFile(String inputFile) {
        List<String> list = new ArrayList<>();
        try (ZipFile zipFile = new ZipFile(inputFile)) {
            Enumeration<ZipArchiveEntry> entries = zipFile.getEntries();
            while (entries.hasMoreElements()) {
                ZipArchiveEntry entry = entries.nextElement();
                if (!entry.isDirectory()) {
                    try (InputStreamReader reader = new InputStreamReader(zipFile.getInputStream(entry))) {
                        String content = new Scanner(reader).useDelimiter("\\Z").next();
                        if (entry.getName().endsWith(".json")) {
                            JSONObject jsonObject = new JSONObject(content);
                            JSONArray jsonArray = jsonObject.getJSONArray("expressions");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                list.add(jsonArray.getString(i));
                            }
                        } else if (entry.getName().endsWith(".xml")) {
                            SAXBuilder saxBuilder = new SAXBuilder();
                            Document doc = saxBuilder.build(new StringReader(content));
                            Element rootElement = doc.getRootElement();
                            List<Element> elements = rootElement.getChildren("expression");

                            for (Element element : elements) {
                                list.add(element.getText());
                            }
                        } else if (entry.getName().endsWith(".txt")) {
                            String line;
                            try (BufferedReader reader1 = new BufferedReader(new InputStreamReader(zipFile.getInputStream(entry)))) {
                                while ((line = reader1.readLine()) != null) {
                                    list.add(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        } catch (IOException | JDOMException e) {
            e.printStackTrace();
        }
        return list;
    }
}
