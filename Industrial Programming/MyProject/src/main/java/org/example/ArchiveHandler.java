package org.example;

import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.io.IOUtils;

import java.io.*;

public class ArchiveHandler {
    public static void archiveFile(String filename) {
        try {
            File inputFile = new File(filename);
            File outputFile = new File(filename + ".zip");

            InputStream is = new FileInputStream(inputFile);
            OutputStream os = new FileOutputStream(outputFile);
            ArchiveOutputStream aos = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.ZIP, os);
            aos.putArchiveEntry(new ZipArchiveEntry(inputFile.getName()));
            IOUtils.copy(is, aos);
            aos.closeArchiveEntry();
            aos.finish();
            is.close();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
