package org.example;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonHandler {
    public static List<String> convertJsonToList(JSONObject jsonObject) {
        JSONArray jsonArray = jsonObject.getJSONArray("expressions");
        List<String> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            list.add(jsonArray.getString(i));
        }
        return list;
    }

    public static JSONObject convertListToJson(List<String> data) {
        JSONArray jsonArray = new JSONArray();
        for (String line : data) {
            jsonArray.put(line);
        }
        return new JSONObject().put("result", jsonArray);
    }
}