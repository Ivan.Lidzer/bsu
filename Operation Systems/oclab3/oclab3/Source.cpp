#include <iostream>
#include <windows.h>
#include <string>
using namespace std;

struct workerInfo {
    string charArray;
    HANDLE eventHandleSort;
    HANDLE eventHandleSum;
    CRITICAL_SECTION cs;
    CRITICAL_SECTION cs2;
    int k;
    double median;
    int mera;
};

void worker(LPVOID params) {
    workerInfo& info = *static_cast<workerInfo*>(params);
    EnterCriticalSection(&info.cs);
    int interval;
    std::cout << "Enter interval(ms): ";
    std::cin >> interval;
    int kol = 0;
    for (int i = 0; i < info.charArray.size(); ++i) {
        if (isdigit(info.charArray[i]))
        {
            swap(info.charArray[i], info.charArray[kol]);
            kol++;
        }
        Sleep(interval);
    }
    info.mera = kol;
    for (int i = kol; i < info.charArray.size(); ++i) {
        info.charArray[i] = ' ';
        Sleep(interval);
    }
    LeaveCriticalSection(&info.cs);
    // SetEvent(info.eventHandleSort);
}

void SumElement(LPVOID params) {
    workerInfo& info = *static_cast<workerInfo*>(params);
    EnterCriticalSection(&info.cs2);
    WaitForSingleObject(info.eventHandleSum, INFINITE);
    info.median = 0;
    if (info.mera < info.k)
    {
        info.k = info.mera;
    }
    for (int i = 0; i < info.k; ++i)
    {
      info.median += (int)info.charArray[i] - (int)('0');
    }
    info.median = info.median / info.k;
    LeaveCriticalSection(&info.cs2);
}


int main()
{
    workerInfo workerData;
    InitializeCriticalSection(&workerData.cs);
    InitializeCriticalSection(&workerData.cs2);

    workerData.eventHandleSort = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (workerData.eventHandleSort == NULL) {
        return GetLastError();
    }

    cout << "Enter mas: ";
    getline(std::cin, workerData.charArray);
    cout << "Array size: " << workerData.charArray.size() << '\n';
    cout << "Mas: ";
    cout << workerData.charArray << '\n';
    HANDLE hThread[2];
    DWORD dwThread[2];
    hThread[0] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)worker,
        (LPVOID)(&workerData), CREATE_SUSPENDED, &dwThread[0]);
    if (hThread[0] == NULL) {
        return GetLastError();
    }
    hThread[1] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)SumElement,
        (LPVOID)(&workerData), CREATE_SUSPENDED, &dwThread[1]);
    if (hThread[1] == NULL) {
        return GetLastError();
    }
    cout << "Enter k: ";
    cin >> workerData.k;
    ResumeThread(hThread[0]);
    ResumeThread(hThread[1]);
    workerData.eventHandleSum = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (workerData.eventHandleSum == NULL) {
        return GetLastError();
    }
    Sleep(2);



   // WaitForSingleObject(workerData.eventHandleSort, INFINITE);

    EnterCriticalSection(&workerData.cs);
    cout << "\n Sorted Array(first " << workerData.k << " symbolds): ";
    for (int i = 0; i < workerData.k; ++i) {
        cout << workerData.charArray[i];
    }
    LeaveCriticalSection(&workerData.cs);


    SetEvent(workerData.eventHandleSum);
    EnterCriticalSection(&workerData.cs2);
    cout << "\n sumElement result : " << workerData.median;
    LeaveCriticalSection(&workerData.cs2);
    WaitForSingleObject(hThread[0], INFINITE);

    DeleteCriticalSection(&workerData.cs);
    DeleteCriticalSection(&workerData.cs2);
    CloseHandle(hThread[0]);
    CloseHandle(hThread[1]);
    CloseHandle(workerData.eventHandleSort);
    CloseHandle(workerData.eventHandleSum);
	return 0;
}