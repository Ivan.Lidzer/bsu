#include <iostream>
#include <ctime> 
#include <windows.h>
using namespace std;


struct workerParams {
	short int* mass;
	int size;
};

DWORD WINAPI worker(LPVOID ptParams)
{
	workerParams* params = (workerParams*)ptParams;
	cout << "ne chet el-ti: ";
	for (int i = 0; i < params->size; i++)
	{
		if (params->mass[i] % 2 != 0)
		{
			cout << params->mass[i]<<" ";
		}
		Sleep(50);
	}
	return 0;
}

void processArray(workerParams* params) {
	for (int i = 0; i < params->size; i++)
	{
		if (params->mass[i] % 2 != 0)
		{
			cout << params->mass[i] << " ";
		}
		Sleep(50);
	}
}


int main()
{
	HANDLE hThread;
	DWORD IDThread;

	workerParams params;
	int n;
	cout << "Vvtdite kol-ov el-ov massiva: ";
	cin >> params.size;
	params.mass = new short int[params.size];
	srand(time(NULL));
	cout << "massiv: ";
	for (int i = 0; i < params.size; i++)
	{
		params.mass[i] = (short)rand() % 10;
		cout << params.mass[i] << " ";
	}
	cout << endl;
	hThread = CreateThread(NULL, 0, worker, (void*)(&params), 0, &IDThread);
	if (hThread == NULL)
		return GetLastError();

	SuspendThread(hThread);
	Sleep(30);
	ResumeThread(hThread);
	WaitForSingleObject(hThread, INFINITE);
	CloseHandle(hThread);

	return 0;
}