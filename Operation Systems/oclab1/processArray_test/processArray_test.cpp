#include "pch.h"
#include "CppUnitTest.h"
#include "../oclab1/Source.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace processArraytest
{
    TEST_CLASS(processArraytest)
    {
    public:

		TEST_METHOD(TestMethod1)
		{
			workerParams params;
			params.size = 5;
			params.mass = new short int[params.size]{ 1, 2, 3, 4, 5 };
			std::ostringstream oss;
			std::streambuf* pCout = std::cout.rdbuf();
			std::cout.rdbuf(oss.rdbuf());
			processArray(&params);
			std::string str = oss.str();
			std::cout.rdbuf(pCout);
			Assert::AreEqual(std::string("1 3 5 "), str);
		}

		TEST_METHOD(TestMethod2)
		{
			workerParams params;
			params.size = 0;
			params.mass = new short int[params.size];
			std::ostringstream oss;
			std::streambuf* pCout = std::cout.rdbuf();
			std::cout.rdbuf(oss.rdbuf());
			processArray(&params);
			std::string str = oss.str();
			std::cout.rdbuf(pCout);
			Assert::AreEqual(std::string(""), str);
		}

		TEST_METHOD(TestMethod3)
		{
			workerParams params;
			params.size = 1;
			params.mass = new short int[params.size]{ 1 };
			std::ostringstream oss;
			std::streambuf* pCout = std::cout.rdbuf();
			std::cout.rdbuf(oss.rdbuf());
			processArray(&params);
			std::string str = oss.str();
			std::cout.rdbuf(pCout);
			Assert::AreEqual(std::string("1 "), str);
		}
    };
}