#include <iostream>
#include <windows.h>
#include <string>

using namespace std;


int main(int argc, char* argv[])
{
	HANDLE hMutex = OpenMutex(MUTEX_ALL_ACCESS,FALSE, "WriterMutex");
	HANDLE writerEvents[2];
	writerEvents[0] = OpenEvent(EVENT_MODIFY_STATE, FALSE, "WriterEndEvent");
	if (writerEvents[0] == NULL || hMutex == NULL)
	{
		return GetLastError();
	}


	HANDLE readerEvents[2];
	for (int i = 0; i < 2; i++)
	{
		readerEvents[i] = OpenEvent(EVENT_ALL_ACCESS, FALSE, ("ReaderEvent" + to_string(i)).c_str());
		if (readerEvents[i] == NULL)
		{
			return GetLastError();
		}
	}

	int massageNum = atoi(argv[0]);
	WaitForSingleObject(hMutex, INFINITE);
	for (int i = 0; i < massageNum; i++)
	{
		char message;
		cout << "Enter mwssage A,B: ";
		cin >> message;
		switch (message) {
		case 'A':
			SetEvent(readerEvents[0]);
			break;
		case 'B':
			SetEvent(readerEvents[1]);
			break;
		}
	}
	PulseEvent(writerEvents[0]);
	ReleaseMutex(hMutex);
	string temp;
	getline(cin, temp);
	getline(cin, temp);
	CloseHandle(hMutex);
	for (int i = 0; i < 2; i++)
	{
		CloseHandle(readerEvents[i]);
	}
	CloseHandle(writerEvents[0]);
	return 0;
}