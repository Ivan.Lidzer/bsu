#include <iostream>
#include <windows.h>
#include <string>

using namespace std;


int main(int argc, char* argv[])
{
	HANDLE hSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, FALSE, "ReaderSemaphore");
	HANDLE readerEvents[2];
	HANDLE writerEvents[2];

	for (int i = 0; i < 2; i++)
	{
		readerEvents[i] = OpenEvent(EVENT_ALL_ACCESS, FALSE, ("ReaderEvent" + to_string(i)).c_str());
		if (readerEvents[i] == NULL)
		{
			return GetLastError();
		}
	}

	writerEvents[1] = OpenEvent(EVENT_MODIFY_STATE, FALSE, "ReaderEndEvent");
	if (readerEvents[2] == NULL || hSemaphore == NULL)
	{
		return GetLastError();
	}
	int massageNum = atoi(argv[0]);
	WaitForSingleObject(hSemaphore, INFINITE);
	cout << "active \n";
	while (massageNum)
	{
		DWORD message = WaitForMultipleObjects(2, readerEvents, FALSE, INFINITE);
		switch (message)
		{
		case 0:
			cout << "message: A\n";
			ResetEvent(readerEvents[0]);
			break;
		case 1:
			cout << "message: B\n";
			ResetEvent(readerEvents[1]);
			break;
		}
		--massageNum;
	}
	ReleaseSemaphore(hSemaphore, 1, NULL);
	string temp;
	getline(cin, temp);
	PulseEvent(writerEvents[1]);
	CloseHandle(hSemaphore);
	for (int i = 0; i < 2; i++)
	{
		CloseHandle(readerEvents[i]);
	}
	CloseHandle(writerEvents[1]);
	return 0;
}