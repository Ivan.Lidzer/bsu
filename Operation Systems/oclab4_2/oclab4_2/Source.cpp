#include <iostream>
#include <windows.h>
#include <string>

using namespace std;


int main()
{
    HANDLE hMutex = CreateMutex(NULL, FALSE, "WriterMutex");
    HANDLE hSemaphore = CreateSemaphore(NULL, 2, 2, "ReaderSemaphore");
    if (hMutex == NULL || hSemaphore == NULL) {
        return GetLastError();
    }
    HANDLE writerEvents[2];
    HANDLE readerEvents[2];

    for (int i = 0; i < 2; ++i) {
        readerEvents[i] = CreateEvent(NULL, TRUE, FALSE, ("ReaderEvent" + to_string(i)).c_str());
        if (readerEvents[i] == NULL) {
            return GetLastError();
        }
    }
    writerEvents[1] = CreateEvent(NULL, FALSE, FALSE, "ReaderEndEvent");
    writerEvents[0] = CreateEvent(NULL, FALSE, FALSE, "WriterEndEvent");
    int processNum, messageNum;
    cout << "Enter number of Writer(2*Reader) processes: ";
    cin >> processNum;
    cout << "Enter number of messages: ";
    cin >> messageNum;
    STARTUPINFO si;
    PROCESS_INFORMATION* writer_pi = new PROCESS_INFORMATION[processNum];
    PROCESS_INFORMATION* reader_pi = new PROCESS_INFORMATION[2*processNum];
    string writerInfo = to_string(messageNum);

    for (int i = 0; i < processNum; ++i) {
        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);
        if (!CreateProcess("C:\\Users\\vanya\\source\\repos\\oclab4_2\\Debug\\Writer.exe", (char*)writerInfo.c_str(), NULL, NULL, FALSE,
            CREATE_NEW_CONSOLE, NULL, NULL, &si, &writer_pi[i])) {
            cout << "\nwriter process is not created\n";
            return GetLastError();
        }
    }
    for (int i = 0; i < 2*processNum; ++i) {
        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);

        if (!CreateProcess("C:\\Users\\vanya\\source\\repos\\oclab4_2\\Debug\\Reader.exe", (char*)writerInfo.c_str(), NULL, NULL, FALSE,
            CREATE_NEW_CONSOLE, NULL, NULL, &si, &reader_pi[i])) {
            cout << "\nreader process is not created\n";
            return GetLastError();
        }
    }

    int writerNum = processNum;
    int readerNum = 2*processNum;
    while (writerNum || readerNum) {
        DWORD message1 = WaitForMultipleObjects(2, writerEvents, FALSE, INFINITE);
        switch (message1) {
        case WAIT_OBJECT_0:
            cout << "Writer Process ended\n";
            --writerNum;
            break;
        case WAIT_OBJECT_0 + 1:
            cout << "2*Reader Process ended\n";
            readerNum = readerNum - 2;
            break;
        }
    }

    CloseHandle(hMutex);
    CloseHandle(hSemaphore);
    for (int i = 0; i < 2; ++i) {
        CloseHandle(readerEvents[i]);
    }
    for (int i = 0; i < 2; ++i) {
        CloseHandle(writerEvents[i]);
    }
    for (int i = 0; i < processNum; ++i) {
        CloseHandle(writer_pi[i].hThread);
        CloseHandle(writer_pi[i].hProcess);
    }
    for (int i = 0; i < 2*processNum; ++i) {
        CloseHandle(reader_pi[i].hThread);
        CloseHandle(reader_pi[i].hProcess);
    }
    return 0;
}
