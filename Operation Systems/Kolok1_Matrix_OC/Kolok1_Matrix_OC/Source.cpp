﻿#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <random>

std::mutex mtx;

void multiply(const std::vector<std::vector<int>>& A, const std::vector<std::vector<int>>& B, std::vector<std::vector<int>>& C, int start, int end) {
    for (int i = start; i < end; ++i) {
        for (int j = 0; j < B[0].size(); ++j) {
            for (int k = 0; k < A[0].size(); ++k) {
                mtx.lock();
                C[i][j] += A[i][k] * B[k][j];
                mtx.unlock();
            }
        }
    }
}

int main() {
    int m, n, k, p;
    std::cout << "A (mXn) and B (nXk) and C=AxB => C (mXk) and p number of threads" << '\n' << "Input m,n,k,p : ";
    std::cin >> m >> n >> k >> p;
    std::vector<std::vector<int>> A(m, std::vector<int>(n));
    std::vector<std::vector<int>> B(n, std::vector<int>(k));
    std::vector<std::vector<int>> C(m, std::vector<int>(k, 0));

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(1, 10);

    for (auto& row : A)
        for (auto& elem : row)
            elem = dis(gen);

    for (auto& row : B)
        for (auto& elem : row)
            elem = dis(gen);

    std::cout << "Matrix A:" << '\n';
    for (const auto& row : A) {
        for (const auto& elem : row) {
            std::cout << elem << ' ';
        }
        std::cout << '\n';
    }

    std::cout<<'\n' << "Matrix B:" << '\n';
    for (const auto& row : B) {
        for (const auto& elem : row) {
            std::cout << elem << ' ';
        }
        std::cout << '\n';
    }

    std::vector<std::thread> threads;
    int rows_per_thread = m / p;
    int remainder_rows = m % p;

    for (int i = 0; i < p; ++i) {
        threads.push_back(std::thread(multiply, std::ref(A), std::ref(B), std::ref(C), i * rows_per_thread, (i + 1) * rows_per_thread));
    }

    if (remainder_rows > 0) {
        threads.push_back(std::thread(multiply, std::ref(A), std::ref(B), std::ref(C), p * rows_per_thread, m));
    }

    for (auto& th : threads) th.join();

    std::cout << '\n' << "Matrix C:" << '\n';
    for (const auto& row : C) {
        for (const auto& elem : row) {
            std::cout << elem << ' ';
        }
        std::cout << '\n';
    }

    return 0;
}
