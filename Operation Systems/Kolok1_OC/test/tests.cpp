#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <fstream>
#include "../Kolok1_OC/Source.cpp"

void write_test_file(const std::string& filename, int operation, const std::vector<double>& numbers) {
    std::ofstream file(filename);
    file << operation << std::endl;
    for (const auto& num : numbers) {
        file << num << " ";
    }
    file.close();
}

TEST_CASE("ProcessFileTest") {
    SECTION("TestSum") {
        double total = 0;
        write_test_file("test1.dat", 1, { 1, 2, 3, 4, 5 });
        process_file("test1.dat", total);
        REQUIRE(total == Approx(15));
    }

    SECTION("TestProduct") {
        double total = 0;
        write_test_file("test2.dat", 2, { 1, 2, 3, 4, 5 });
        process_file("test2.dat", total);
        REQUIRE(total == Approx(120));
    }

    SECTION("TestSquares") {
        double total = 0;
        write_test_file("test3.dat", 3, { 1, 2, 3, 4, 5 });
        process_file("test3.dat", total);
        REQUIRE(total == Approx(55));
    }
}
