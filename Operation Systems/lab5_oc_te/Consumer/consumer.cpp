#include <iostream>
#include <windows.h>
#include <conio.h>
#include <vector>

using namespace std;

int main(int argc, char* argv[]) {

    HANDLE hReadPipe, hWritePipe;

    sscanf(argv[0], "%p", &hWritePipe);
    sscanf(argv[1], "%p", &hReadPipe);
    cout << "hReadPipe: " << hReadPipe << endl;
    int arl1;
    int arl2;

    DWORD dwBytesWrite, dwBytesRead;

    if (!ReadFile(hReadPipe, &arl1, sizeof(int), &dwBytesRead, NULL)) {
        DWORD error = GetLastError();

        LPVOID errorMsgBuffer = nullptr;

        DWORD result = FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,nullptr,error,0,reinterpret_cast<LPSTR>((LPWSTR)&errorMsgBuffer),0, nullptr );

        if (result != 0) {
            wprintf(L"Read failed with error %lu: %s\n", error, errorMsgBuffer);

            LocalFree(errorMsgBuffer);
        }
        else {
            std::cerr << "Read failed with error code " << error << std::endl;
        }

        return error;
    }

    char* str = new char[arl1];
    if (!ReadFile(hReadPipe, str, sizeof(char) * arl1, &dwBytesRead, NULL))
    {
        cout << "array  read is failed " << endl;
        return GetLastError();
    }


    if (!ReadFile(hReadPipe, &arl2, sizeof(int), &dwBytesRead, NULL))
    {
        cout << "array2 length read is failed " << endl;
        return GetLastError();
    }

    int* arr = new int[arl2];
    if (!ReadFile(hReadPipe, arr, sizeof(int) * arl2, &dwBytesRead, NULL))
    {
        cout << "array 2 read is failed " << endl;
        return GetLastError();
    }

    cout << "Received Array 1 from Server:" << endl;
    for (int i = 0; i < arl1; ++i) {
        cout << str[i] << " ";
    }
    cout << endl;

    cout << "Received Array 2 from Server:" << endl;
    for (int i = 0; i < arl2; ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;


    std::vector<char> commonElements;
    for (int i = 0; i < arl1; ++i) {
        for (int j = 0; j < arl2; ++j) {
            if (str[i] - '0' == arr[j]) {
                commonElements.push_back(str[i]);
            }
        }
    }

    cout << "Common elements:" << endl;
    auto new_size = commonElements.size();
    char* res = new char[new_size];
    for (int i = 0; i < new_size; ++i) {
        cout << commonElements[i] << " ";
        res[i] = commonElements[i];
    }
    cout << endl;

    if (!WriteFile(hWritePipe, &new_size, sizeof(int), &dwBytesWrite, NULL)) {
        cout << "Write of new size failed\n";
        return GetLastError();
    }
    if (!WriteFile(hWritePipe, res, new_size * sizeof(char), &dwBytesWrite, NULL)) {
        cout << "Write of new array failed\n";
        return GetLastError();
    }

    _cprintf("\nTo exit press any key ");
    _getch();

    delete[] str;
    CloseHandle(hWritePipe);
    CloseHandle(hReadPipe);

    return 0;
}
