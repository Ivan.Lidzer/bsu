
#include <iostream>
#include <windows.h>

using namespace std;

int main() {
    cout << "Enter the length of the first array: ";
    int arrayLength1;
    cin >> arrayLength1;

    char* str = new char[arrayLength1];

    cout << "Enter the first array elements: ";
    for (int i = 0; i < arrayLength1; ++i) {
        cin >> str[i];
    }
    cout << "\nInputed array: ";
    for (int i = 0; i < arrayLength1; ++i) {
        cout << str[i] << " ";
    }
    cout << endl;
    cout << "Enter the length of the second array: ";
    int arrayLength2;
    cin >> arrayLength2;

    int* arr = new int[arrayLength2];

    cout << "Enter the second array elements: ";
    for (int i = 0; i < arrayLength2; ++i) {
        cin >> arr[i];
    }
    cout << "\nInputed array: ";
    for (int i = 0; i < arrayLength2; ++i) {
        cout << arr[i] << " ";
    }
    cout << "\n";

    HANDLE hWritePipe, hReadPipe, hInheritWritePipe, hInheritReadPipe;


    SECURITY_ATTRIBUTES saAttr;
    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
    saAttr.bInheritHandle = FALSE;
    saAttr.lpSecurityDescriptor = NULL;

    if (hReadPipe == INVALID_HANDLE_VALUE || hWritePipe == INVALID_HANDLE_VALUE) {
        cout << "Error creating pipe. Error: " << GetLastError() << endl;
        return GetLastError();
    }
    if (!CreatePipe(&hReadPipe, &hWritePipe, &saAttr, 0)) {
        cout << "Create pipe failed" << endl;
        return GetLastError();
    }
    if (!DuplicateHandle(GetCurrentProcess(), hWritePipe, GetCurrentProcess(), &hInheritWritePipe, 0, TRUE, DUPLICATE_SAME_ACCESS))
    {
        cout << "duplicate write pipe is failed" << endl;
        return GetLastError();
    }
    if (!DuplicateHandle(GetCurrentProcess(), hReadPipe, GetCurrentProcess(), &hInheritReadPipe, 0, TRUE, DUPLICATE_SAME_ACCESS))
    {
        cout << "duplicate write pipe is failed" << endl;
        return GetLastError();
    }

    DWORD dwBytesWrite;
    if (!WriteFile(hInheritWritePipe, &arrayLength1, sizeof(int), &dwBytesWrite, NULL)) {
        cout << "Write of length of first array failed\n" << GetLastError();
        return GetLastError();
    }


    if (!WriteFile(hInheritWritePipe, str, arrayLength1 * sizeof(char), &dwBytesWrite, NULL)) {
        cout << "Write of first array failed\n" << GetLastError();
        return GetLastError();
    }

    if (!WriteFile(hInheritWritePipe, &arrayLength2, sizeof(int), &dwBytesWrite, NULL)) {
        cout << "Write of second length failed\n" << GetLastError();
        return GetLastError();
    }


    if (!WriteFile(hInheritWritePipe, arr, arrayLength2 * sizeof(int), &dwBytesWrite, NULL)) {
        cout << "Write of second array failed\n" << GetLastError();
        return GetLastError();
    }


    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    char lpszComLine[80];
    wsprintf(lpszComLine, "%p %p", hInheritWritePipe, hInheritReadPipe);

    ZeroMemory(&si, sizeof(si));
    ZeroMemory(&pi, sizeof(pi));
    si.cb = sizeof(si);

    if (!CreateProcess("Consume.exe", lpszComLine,
        NULL, NULL, TRUE,
        CREATE_NEW_CONSOLE,
        NULL, NULL,
        &si, &pi))
    {
        cout << "Consume.exe was not launched.\n";
        return GetLastError();
    }


    WaitForSingleObject(pi.hProcess, INFINITE);

    int new_size2;
    DWORD byteRwad;
    if (!ReadFile(hInheritReadPipe, &new_size2, sizeof(int), &byteRwad, NULL))
    {
        cout << "New size 2 read is failed " << endl;
        return GetLastError();
    }
    char* arr1 = new char[new_size2];
    if (!ReadFile(hInheritReadPipe, arr1, sizeof(char) * new_size2, &byteRwad, NULL))
    {
        cout << "New array read 2 is failed " << endl;
        return GetLastError();
    }


    cout << "\nArray of common elements:\n";
    for (int i = 0; i < new_size2; i++)
    {
        cout << arr1[i] << " ";
    }
    CloseHandle(hReadPipe);
    CloseHandle(hWritePipe);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    delete[] str;
    delete[] arr;
    delete[] arr1;

    return 0;
}
