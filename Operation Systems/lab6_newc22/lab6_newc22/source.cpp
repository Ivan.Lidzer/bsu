#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
using namespace std;

struct workerInfo {
    string charArray;
    int k;
    double median;
    int mera;
};

std::mutex mtx1,mtx2;
std::condition_variable cv;
bool ready = false;

void worker(workerInfo& info) {
    mtx1.lock();
    int interval;
    std::cout << "Enter interval(ms): ";
    std::cin >> interval;
    int kol = 0;
    for (int i = 0; i < info.charArray.size(); ++i) {
        if (isdigit(info.charArray[i]))
        {
            swap(info.charArray[i], info.charArray[kol]);
            kol++;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(interval));
    }
    info.mera = kol;
    for (int i = kol; i < info.charArray.size(); ++i) {
        info.charArray[i] = ' ';
        std::this_thread::sleep_for(std::chrono::milliseconds(interval));
    }
    mtx1.unlock();
    cout << "worker end";
}

void SumElement(workerInfo& info) {
    mtx2.lock();
    info.median = 0;
    if (info.mera < info.k)
    {
        info.k = info.mera;
    }
    for (int i = 0; i < info.k; ++i)
    {
        info.median += (int)info.charArray[i] - (int)('0');
    }
    info.median = info.median / info.k;
    ready = true;
    cv.notify_one();
    mtx2.unlock();
    cout << "sum end";
}

int main()
{
    workerInfo workerData;

    cout << "Enter mas: ";
    getline(std::cin, workerData.charArray);
    cout << "Array size: " << workerData.charArray.size() << '\n';
    cout << "Mas: ";
    cout << workerData.charArray << '\n';
    cout << "Enter k: ";
    cin >> workerData.k;
    mtx2.lock();
    std::thread workerThread(worker, std::ref(workerData));
    std::thread sumThread(SumElement, std::ref(workerData));
 
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    mtx1.lock();
    cout << "\n Sorted Array(first " << workerData.k << " symbolds): ";
    for (int i = 0; i < workerData.k; ++i) {
            cout << workerData.charArray[i];
    }
    mtx1.unlock();
    mtx2.unlock();
    std::unique_lock<std::mutex> lock(mtx2);
    cv.wait(lock, [] {return ready; });
    cout << "\n sumElement result : " << workerData.median;
    


    workerThread.join();
    sumThread.join();
    return 0;
}
