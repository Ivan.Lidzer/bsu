#include <iostream>
#include <queue>
#include <thread>
#include <condition_variable>
#include <chrono>

std::queue<int> buffer;
const unsigned int buffer_size = 10;
std::mutex m;
std::condition_variable cv;

void producer() {
    int i = 0;
    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::unique_lock<std::mutex> lock(m);
        cv.wait(lock, [] {return buffer.size() != buffer_size; });
        std::cout << "Producing " << i << std::endl;
        buffer.push(i++);
        cv.notify_all();
    }
}

void consumer() {
    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::unique_lock<std::mutex> lock(m);
        cv.wait(lock, [] {return buffer.size() != 0; });
        std::cout << "Consuming " << buffer.front() << std::endl;
        buffer.pop();
        cv.notify_all();
    }
}

int main() {
    std::thread t1(producer);
    std::thread t2(consumer);
    t1.join();
    t2.join();
    return 0;
}
